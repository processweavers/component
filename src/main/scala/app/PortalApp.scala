package app

import java.net.InetAddress

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import web.MyRoute

object RemoteComponentApp {

  implicit val system = ActorSystem("portal-component")
  implicit val materializer = ActorMaterializer()

  val port = 8444
  val interface = "0.0.0.0"

  def main(args: Array[String]): Unit = {
    system.log.info("Starting web server on host '{}' listening at {}:{}", InetAddress.getLocalHost.getHostName, interface, port)
    Http().bindAndHandle(new MyRoute().route, interface, port)
  }
}
