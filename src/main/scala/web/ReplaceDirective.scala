package web

import akka.http.scaladsl.model.HttpEntity
import akka.http.scaladsl.server.Directive0
import akka.http.scaladsl.server.directives._
import akka.stream.scaladsl.Framing
import akka.util.ByteString

import scala.util.Properties

trait ReplaceDirective extends BasicDirectives {

  def token(x: String): (String, String) = x -> fromEnv(x, s"{$x not resolved}")

  def fromEnv(n: String, defaultText: String): String = Properties.envOrElse(n, defaultText)

  def replace(m: Map[String, String]): Directive0 =
    mapResponseEntity {
      case entity @ HttpEntity.Default(ct, length, data) =>
        val replaced = data
          .via(Framing.delimiter(ByteString("\n"), Int.MaxValue, allowTruncation = true))
          .map(line => ByteString(replacePlaceHolder(line.utf8String, m)))
          .intersperse(ByteString("\n"), ByteString("\n"), ByteString("\n"))
        HttpEntity.CloseDelimited(ct, replaced)
      case entity @ HttpEntity.Strict(ct, data) =>
        HttpEntity.Strict(ct, ByteString(replacePlaceHolder(data.utf8String, m)))
      case entity => throw new RuntimeException(s"Invalid http entity type $entity")
    }

  private def replacePlaceHolder(s: String, replacements: Map[String, String]): String =
    replacements.foldLeft(s) { case (t, (k, v)) => t.replace(k, v) }
}
