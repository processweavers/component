import com.typesafe.sbt.SbtNativePackager.Docker
import com.typesafe.sbt.packager.docker._

enablePlugins(JavaAppPackaging)

lazy val akkaHttpVersion = "10.0.11"
lazy val akkaVersion    = "2.5.7"

lazy val releaseSettings = Seq(
  publishMavenStyle := true,
  pomIncludeRepository := { _ => false },
  publishArtifact in Test := false,
)

lazy val root = (project in file("."))
  .settings(
      concurrentRestrictions in Global += Tags.limit(Tags.Test, 1),
      inThisBuild(List(
      organization    := "net.processweavers",
      version := "0.0.1-SNAPSHOT",
      scalaVersion    := "2.12.4",
      scalacOptions ++= Seq(
        "-unchecked",
        "-deprecation",
        "-Xexperimental",
        "-feature",
        "-language:implicitConversions",
        "-language:reflectiveCalls"
      )
    )),
    name := "portal-component",
    libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-http"  % akkaHttpVersion,
      "org.scalatest" %% "scalatest"      % "3.0.5"         % "test"
    ),
    fork in run := true
  )
  .settings(
    dockerBaseImage := "openjdk:8",
    dockerExposedPorts := Seq(8444),
    daemonUser in Docker := "root",
    dockerRepository := Some("dockerRepo:5000")
  )
